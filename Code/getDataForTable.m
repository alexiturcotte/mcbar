% here, r should contain the test results

chartVals = zeros(4, 10, 2);

for i = 1: 4
    for j = 1: 10
        chartVals(i, j, 1) = r(1, i, j).avgError;
        chartVals(i, j, 2) = r(2, i, j).avgError;
    end
end