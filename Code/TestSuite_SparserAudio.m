% TestSuite_SparserAudio: test algorithms on more sparse audio files.
%
% The only difference in this test suite is that we reduce the length of
% the signals by sampling every 10th point. These results do not feature
% in our report.

clc;

disp( 'Initializing.');

funsToTestWith{1} = @(s, t) harvilla(s, t);
funsToTestWith{2} = @(s, t) harvilla_centered_fudge(s, t);
funsToTestWith{3} = @(s, t) harvilla_centered_smarter(s, t);
funsToTestWith{4} = @(s, t) harvilla_centered_smarter_endfix(s, t);
funsToTestWith{5} = @(s, t) declip(s, t, 'bp');
funsToTestWith{6} = @(s, t) declip(s, t, 'bpcc');
funsToTestWith{7} = @(s, t) declip(s, t, 'rw_ell1');
funsToTestWith{8} = @(s, t) declip(s, t, 'tpcc');
funNameList = {'harvilla' 'harvilla_centFudge' 'harvilla_centSmart' 'harvilla_centEndfix' 'basisPursuit' 'basisPursuitCC' 'RWELL1' 'tcpp'};

emptyStruct.declipped = [];
emptyStruct.errors = [];
emptyStruct.avgError = 0;
emptyStruct.origData = [];
emptyStruct.clipped = [];
emptyStruct.thresh = 0;
emptyStruct.funName = '';
emptyStruct.dataName = '';

threshs = [0.6 0.8]; % 0.2 and 0.4 just take too long
dataListName = {'c4' 'c4_fifth_ldsynth' 'c4_organ' 'c4_piccolo' 'c4_strings' 'chord_fifth' 'chord_organ' 'chord_piccolo' 'chord_grand' 'chord_strings' 'song' 'song_chronicles'};
dataList = {'../DataSamples/shorter_samples/c4.wav' '../DataSamples/shorter_samples/c4_fifth_ld_synth.wav' '../DataSamples/shorter_samples/c4_organ.wav' '../DataSamples/shorter_samples/c4_piccolo.wav' '../DataSamples/shorter_samples/c4_strings.wav' '../DataSamples/shorter_samples/chord_fifth.wav' '../DataSamples/shorter_samples/chord_organ.wav' '../DataSamples/shorter_samples/chord_piccolo.wav' '../DataSamples/shorter_samples/chord_st_grand.wav' '../DataSamples/shorter_samples/chord_strings.wav' '../DataSamples/shorter_samples/song_snippet.wav' '../DataSamples/shorter_samples/song_snippet_chronicles.wav'};

Results_Sparser = repmat( emptyStruct, length( funsToTestWith), length( threshs), length( dataListName));

disp( 'Starting tests.');

for d = 1:length( dataList)

    fprintf( '%s: Begin.\n', dataListName{ d});

    curData = audioread( dataList{ d});

    curData = curData( 1: 10: length( curData)); % shorten by 10x
    
    % normalize it so the cutoff always makes sense as a percentage 
    % of the curve.
    curData = curData / max( abs( curData));  

    for f = 1:length( funsToTestWith)
        
        fprintf( '%s: Begin.\n', funNameList{f});
        
        for t = 1:length( threshs)
            
            thresh = threshs( t);

            fprintf( 'Threshold = %f: Begin.\n', thresh);
            
            % clip
            clipSig = curData;
            clipSig( find( clipSig > thresh)) = thresh;
            clipSig( find( clipSig < -thresh)) = -thresh;

            % declip
            declipped = funsToTestWith{ f}( clipSig, thresh);
            
            % compute errors
            errors = squaredErrors( curData, clipSig, declipped);
            avgError = mean( errors);

            % pack results
            Results_Sparser( f, t, d).declipped = declipped;
            Results_Sparser( f, t, d).errors = errors;
            Results_Sparser( f, t, d).avgError = avgError;
            Results_Sparser( f, t, d).origData = curData;
            Results_Sparser( f, t, d).clipped = clipSig;
            Results_Sparser( f, t, d).thresh = thresh;
            Results_Sparser( f, t, d).funName = funNameList{ f};
            Results_Sparser( f, t, d).dataName = dataListName{ d};
        end
    end
end
