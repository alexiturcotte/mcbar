% TestSuite_noBasics: Testing McBAR and Windowed Reweighted Minimization
%
% This test suite compares McBAR and Windowed RWM on the audio clips.
% For each signal, compare declipping algorithms at clipping thresholds of
% 0.2, 0.4, 0.6, and 0.8.
%
% Results are available in the Results_noBasics structure.

clc;

disp( 'Initializing.');

funsToTestWith{1} = @(s, t) rwm_windowed(s, t);
funsToTestWith{2} = @(s, t) harvilla_centered_smarter_endfix(s, t);
funNameList = {'RWELL1_Windowed' 'harvilla_centEndfix'};

emptyStruct.declipped = [];
emptyStruct.errors = [];
emptyStruct.avgError = 0;
emptyStruct.origData = [];
emptyStruct.clipped = [];
emptyStruct.thresh = 0;
emptyStruct.funName = '';
emptyStruct.dataName = '';

threshs = [0.2 0.4 0.6 0.8]; % 0.2 and 0.4 just take too long
dataListName = {'c4' 'c4_fifth_ldsynth' 'c4_organ' 'c4_piccolo' 'c4_strings' 'chord_fifth' 'chord_organ' 'chord_piccolo' 'chord_grand' 'chord_strings' 'song' 'song_chronicles'};
dataList = {'../DataSamples/shorter_samples/c4.wav' '../DataSamples/shorter_samples/c4_fifth_ld_synth.wav' '../DataSamples/shorter_samples/c4_organ.wav' '../DataSamples/shorter_samples/c4_piccolo.wav' '../DataSamples/shorter_samples/c4_strings.wav' '../DataSamples/shorter_samples/chord_fifth.wav' '../DataSamples/shorter_samples/chord_organ.wav' '../DataSamples/shorter_samples/chord_piccolo.wav' '../DataSamples/shorter_samples/chord_st_grand.wav' '../DataSamples/shorter_samples/chord_strings.wav' '../DataSamples/shorter_samples/song_snippet.wav' '../DataSamples/shorter_samples/song_snippet_chronicles.wav'};

Results_noBasics = repmat( emptyStruct, length( funsToTestWith), length( threshs), length( dataListName));

disp( 'Starting tests.');

for d = 1:length( dataList) % for each audio signal

    fprintf( '%s: Begin.\n', dataListName{ d});

    curData = audioread( dataList{ d});

    % normalize it so the cutoff always makes sense as a 
    % percentage of the curve.
    curData = curData / max( abs( curData));  
    
    for f = 1:2 % for each algorithm
        
        fprintf( '%s: Begin.\n', funNameList{f});
        
        for t = 1:length( threshs) % for each threshold
            
            thresh = threshs( t);

            fprintf( 'Threshold = %f: Begin.\n', thresh);
            
            % clip the signal
            clipSig = curData;
            clipSig( find( clipSig > thresh)) = thresh;
            clipSig( find( clipSig < -thresh)) = -thresh;

            % declip the signal
            declipped = funsToTestWith{ f}( clipSig, thresh);
            
            % compute error array, and average error
            errors = squaredErrors( curData, clipSig, declipped);
            avgError = mean( errors);

            % pack the result structure
            Results_noBasics( f, t, d).declipped = declipped;
            Results_noBasics( f, t, d).errors = errors;
            Results_noBasics( f, t, d).avgError = avgError;
            Results_noBasics( f, t, d).origData = curData;
            Results_noBasics( f, t, d).clipped = clipSig;
            Results_noBasics( f, t, d).thresh = thresh;
            Results_noBasics( f, t, d).funName = funNameList{ f};
            Results_noBasics( f, t, d).dataName = dataListName{ d};
        end
    end
end
