% Simple cBAR Declipping Procedure
%
% Takes a signal, theSignal, and a clipping threshold, tau, and produces
% out, the declipped signal. Also, optional output includes loi, the list
% if window indices if you are interested in seeing which intervals were
% selected.
function [ out, loi ] = harvilla( theSignal, tau)

    % transpose incoming signal for more straightforward invocation of
    % fmincon minimization.
    theSignal = theSignal';

    lengthOfSig = length( theSignal);
    theMax = max( abs( theSignal));
    
    % fixed window size, specified by user.
    windowSize = 80;
    
    % build the list of windows. the list loi will look like:
    % loi <- [ o, window1End, window2End, ... ].
    loi = 0;
    curPos = windowSize;
    while curPos < lengthOfSig
        
        % add clipped areas to the window -- we don't want a window ending
        % in a clipped region.
        while abs( theSignal( curPos)) == theMax && curPos ~= lengthOfSig
            curPos = curPos + 1;
        end
        
        % fudge factor of 2 to ensure better declipping. this way, there
        % will not be situations in which only one unclipped value is 
        % available at the end of a window.
        if ( abs( theSignal( curPos - 1)) == theMax && curPos < lengthOfSig)
            curPos = curPos + 1;
        end
        
        % add windowEnd to loi.
        loi = [loi ; curPos];
        
        % move curPos up by window size to get it ready for next iteration
        curPos = curPos + windowSize;
        
        % however, if curPos exceeds the end of the signal, set it to be
        % the end of the signal, which will end the loop.
        if ( curPos >= lengthOfSig && loi( length( loi)) ~= lengthOfSig)
            loi = [loi ; lengthOfSig];
        end
    end
    
    % compute the output signal by repeatedly declipping
    out = [];
    for i = 1: length( loi)-1
        % note the range:
        % windowNEnd + 1 = window(N+1)Begin, so:
        % [windowNEnd + 1, window(N+1)Begin] = 
        % [window(N+1)Begin, window(N+1)End].
       ret = harvilla_declip( theSignal( loi(i)+1:loi(i+1)), tau);
       out = [out; ret];
    end
end

