% cBAR Declip
%
% Consumes a signal, theSignal, and a clipping threshold, tau, and produces
% output_args, the declipped signal. This is called by all cBAR variants
% and performs the actual declipping work.
function [ output_args ] = harvilla_declip( theSignal, tau)
    
    % transpose the signal to ease fmincon invocation
    x = theSignal';
    N = length( x); 
    
    % K = total length of the clipped segment(s)
    K = length( find( abs( x) == max( abs( x))));
    
    % K will at least be 1, as there must be a max.
    if K ~= 1
    
        % M = length of the unclipped signal.
        M = N - K;

        % x_r = original samples
        x_r = x( find( abs( x) < max( abs( x))));
        
        % x_c = clipped samples
        x_c = x( find( abs( x) == max( abs( x))));

        % set up identity matrices for minimization
        S_r = eye( N);
        S_c = eye( N);

        % remove rows corresponding to:
        % S_r: clipped segments
        % S_c: non-clipped segments
        S_r( find( abs( x) == max( abs( x))), :) = [];
        S_c( find( abs( x) < max( abs( x))), :) = [];

        % compute second order difference
        % note: this represents the derivative
        e = ones(N,1);
        D_2 = spdiags([e -2*e e], -1:1, N, N);

        % build function F for minimization by fmincon
        F = @( x_c) norm( D_2 * (S_r' * x_r + S_c' * x_c))^2;

        % set up empty constraints to satisfy fmincon
        A = [];
        b = [];
        Aeq = [];
        beq = [];

        % want:   x_c .* sgn( S_c x)       >= tau
        % ====>   tau - x_c .* sgn( S_c x) <= 0
        % G is the nonlinear constrain function, passed to fmincon
        % note: deal needed to encode 2 outputs into G.
        G = @( x_c) deal( tau - x_c .* sign( S_c * x), []);

        % suppress annoying output
        options = optimset('Display', 'off');
        
        % perform minimization, declipped signal will be in estimatedVals.
        % note that we are minimizing x_c, so the output will need to over-
        % write the clipped segments.
        estimatedVals = fmincon( F, x_c, A, b, Aeq, beq, [], [], G, options);

        % build output signal by replacing clipped segments with the now 
        % declipped segments. 
        evi = 1;
        output_args = x;
        for i = 1: length( x)
            if ( abs( output_args( i)) == max( abs( x)))
                output_args( i) = estimatedVals( evi);
                evi = evi + 1;
            end
        end
    else % if K == 1, nothing to declip.
        output_args = theSignal';
    end
    
end

