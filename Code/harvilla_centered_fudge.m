% McBAR Declipping Procedure: iteration w/o endfix, dynamic window
%
% Takes a signal, theSignal, and a clipping threshold, tau, and produces
% out, the declipped signal. 
%
% Final iteration of McBAR. Includes:
%
%   > window centering on clipped segments;
%   > forced buffer of 2
%
function [ out ] = harvilla_centered_fudge( theSignal, tau)

    theSignal = theSignal';

    lengthOfSig = length( theSignal);
    
    % locate all windows
    theRanges = find_ranges( theSignal);
    
    % this implementation forces the buffer size to be 2.
    fudgeFactor = 2;
    
    out = theSignal;
    for i = 1: 2: length( theRanges)
        
        % ensure that indices are within [0, length of signal]
        startIndex = max( theRanges(i) - fudgeFactor, 1);
        endIndex = min( theRanges(i+1) + fudgeFactor, lengthOfSig);
        
        % compute range
        thisRange = startIndex:endIndex;
        
        % declip
        ret = harvilla_declip( theSignal( thisRange), tau);
        
        % build output
        out( thisRange) = ret;
    end
end


% find_ranges <- a signal A
%
% places and returns windows surrounding clipped segments in A.
function centers = find_ranges( A)

    % need to differentiate between positive and negative clipped regions
    posMax = max( A);
    negMax = min( A);

    centers = [];
    indexInCenters = 1;
    
    startOfInterval = 0;
    lookingThroughInterval = false;
    isPosMax = false;
    isNegMax = false;
    
    for i = 1: length( A)
        
        % if we're not looking through an interval ...
        if ~ lookingThroughInterval
            
            % ... and we have found a positively clipped sample, then:
            if A( i) == posMax
                % flag the other part of the for loop
                lookingThroughInterval = true;
                isPosMax = true; % indicates a positive clipped segment
                startOfInterval = i;
                
            % ... and we have found a negatively clipped sample, then:
            elseif A( i) == negMax
                % flag the other part of the for loop
                lookingThroughInterval = true;
                isNegMax = true; % indicates a negative clipped segment
                startOfInterval = i;
            end
        % otherwise, we're looking through an interval.
        else
            % stop if we encounter a non clipped sample, and record 
            % locations in pairs in indexInCenters.
            if (isPosMax && A( i) ~= posMax) || (isNegMax && A( i) ~= negMax)
                centers( indexInCenters) = startOfInterval;
                centers( indexInCenters+1) = i;
                lookingThroughInterval = false;
                isPosMax = false;
                isNegMax = false;
                indexInCenters = indexInCenters + 2;
            end
        end
    end
end