% TestSuite_Functions: Testing all algorithms on simple functions.
%
% This test suite compares all algorithms on simple functions.
% For each signal, compare declipping algorithms at clipping thresholds of
% 0.4, 0.6, and 0.8.
%
% Results are available in the Results_Functions structure.

clc;

disp( 'Initializing.');

funsToTestWith{1} = @(s, t) harvilla(s, t);
funsToTestWith{2} = @(s, t) harvilla_centered_fudge(s, t);
funsToTestWith{3} = @(s, t) harvilla_centered_smarter(s, t);
funsToTestWith{4} = @(s, t) harvilla_centered_smarter_endfix(s, t);
funsToTestWith{5} = @(s, t) declip(s, t, 'bp');
funsToTestWith{6} = @(s, t) declip(s, t, 'bpcc');
funsToTestWith{7} = @(s, t) declip(s, t, 'rw_ell1');
funsToTestWith{8} = @(s, t) declip(s, t, 'tpcc');
funNameList = {'harvilla' 'harvilla_centFudge' 'harvilla_centSmart' 'harvilla_centEndfix' 'basisPursuit' 'basisPursuitCC' 'RWELL1' 'tcpp'};

emptyStruct.declipped = [];
emptyStruct.errors = [];
emptyStruct.avgError = 0;
emptyStruct.origData = [];
emptyStruct.clipped = [];
emptyStruct.thresh = 0;
emptyStruct.funName = '';
emptyStruct.dataName = '';

threshs = [0.4 0.6 0.8];

Results_Functions = repmat( emptyStruct, length( funsToTestWith), length( threshs), 2);

% build the signals
range = 0:0.05:4*pi;

theData(1, :) = sin( range - (1/4)*pi) + cos( 2*(range - (1/4)*pi));
theData(1, :) = theData(1, :) / max( abs( theData(1, :))); % normalize

theData(2, :) = sin( range) + sin( 2*range) + sin(4*range) + sin(8*range);
theData(2, :) = theData(2, :) / max( abs( theData(2, :))); % normalize

disp( 'Starting tests.');

for d = 1:2

    fprintf( 'Function %d: Begin.\n', d);
    
    curData = theData(d, :)';
    
    for f = 1:length( funNameList)
        fprintf( '%s: Begin.\n', funNameList{f});

        for t = 1:length( threshs)

            thresh = threshs( t);

            fprintf( 'Threshold = %f: Begin.\n', thresh);
            
            % clip the data
            clipSig = curData;
            clipSig( find( clipSig >  thresh)) =   thresh;
            clipSig( find( clipSig < -thresh)) = - thresh;

            % declip the signal
            declipped = funsToTestWith{ f}( clipSig, thresh);
            
            % compute errors
            errors = squaredErrors( curData, clipSig, declipped);
            avgError = mean( errors);

            % pack results structure
            Results_Functions( f, t, d).declipped = declipped;
            Results_Functions( f, t, d).errors = errors;
            Results_Functions( f, t, d).avgError = avgError;
            Results_Functions( f, t, d).origData = curData;
            Results_Functions( f, t, d).clipped = clipSig;
            Results_Functions( f, t, d).thresh = thresh;
            Results_Functions( f, t, d).funName = funNameList{ f};
        end
    end
end
