% rwm_windowed: Windowed Reweighted Minimization
%
% Declips signal theSignal by applying Reweighted Minimization in windows
% throughout the data. 
% 
% This was mainly done to speed up the computation. 
function [ out ] = rwm_windowed( theSignal, tau)

    theSignal = theSignal';
    
    % ensures that large swaths of unclipped data are not considered.
    % setting to 50, up to user.
    maxWindowSize = 50;
    
    lengthOfSig = length( theSignal);
    oldLengthOfSig = lengthOfSig;
    
    % endfix :: 
    % compute the slope at the end and forecast an additional point.
    % reigns in crazy declipping in all test cases.
    if abs( theSignal( lengthOfSig)) ~= tau && abs( theSignal( lengthOfSig-1)) == tau
        slope = theSignal( lengthOfSig) - theSignal( lengthOfSig - 1);
        theSignal( lengthOfSig + 1) = theSignal( lengthOfSig) + slope;
        lengthOfSig = lengthOfSig + 1;
    end
    
    % compute the clipping windows
    theRanges = find_ranges( theSignal);
    
    out = theSignal;
    
    % for each pair of indices
    for i = 1: 2: length( theRanges)
        
        % compute left and right shifts
        leftShift = theRanges( i)/2;
        rightShift = (lengthOfSig - theRanges( i+1));
        
        % deal with special cases:
        % here, we can safely look at the last range
        if i ~= 1
            leftShift = ( theRanges( i) - theRanges( i-1))/2;
        end
        
        % here, we can safely look at the next range
        if i+1 ~= length( theRanges)
            rightShift = ( theRanges( i+2) - theRanges( i+1))/2;
        end
        
        % if none of these cases apply, leftShift and rightShift are as
        % they were initialized.
        
        % dont let shifts get out of hand, cap them at maxWindowSize
        leftShift = min( floor( leftShift), maxWindowSize);
        rightShift = min( floor( rightShift), maxWindowSize);
        
        % ensure we don't start before index 1, or after index = length.
        startIndex = max( theRanges(i) - leftShift, 1);
        endIndex = min( theRanges(i+1) + rightShift, lengthOfSig);
        
        % make range
        thisRange = startIndex:endIndex;
        
        % declip w RWM
        ret = declip( theSignal( thisRange)', tau, 'rw_ell1');
        
        % build output
        out( thisRange) = ret;
    end
    
    out = out( 1: oldLengthOfSig);
end


% find_ranges <- a signal A
%
% places and returns windows surrounding clipped segments in A.
function centers = find_ranges( A)

    % need to differentiate between positive and negative clipped regions
    posMax = max( A);
    negMax = min( A);

    centers = [];
    indexInCenters = 1;
    
    startOfInterval = 0;
    lookingThroughInterval = false;
    isPosMax = false;
    isNegMax = false;
    
    for i = 1: length( A)
        
        % if we're not looking through an interval ...
        if ~ lookingThroughInterval
            
            % ... and we have found a positively clipped sample, then:
            if A( i) == posMax
                % flag the other part of the for loop
                lookingThroughInterval = true;
                isPosMax = true; % indicates a positive clipped segment
                startOfInterval = i;
                
            % ... and we have found a negatively clipped sample, then:
            elseif A( i) == negMax
                % flag the other part of the for loop
                lookingThroughInterval = true;
                isNegMax = true; % indicates a negative clipped segment
                startOfInterval = i;
            end
        % otherwise, we're looking through an interval.
        else
            % stop if we encounter a non clipped sample, and record 
            % locations in pairs in indexInCenters.
            if (isPosMax && A( i) ~= posMax) || (isNegMax && A( i) ~= negMax)
                centers( indexInCenters) = startOfInterval;
                centers( indexInCenters+1) = i;
                lookingThroughInterval = false;
                isPosMax = false;
                isNegMax = false;
                indexInCenters = indexInCenters + 2;
            end
        end
    end
end