% squaredErrors <- original data, clipped data, declipped data
%
% computes the squared difference between the original and clipped data,
% paying close attention to only calculate at the indices of clipped vals,
% otherwise the error will be closer to 0 than it should be.
function [errors] = squaredErrors( origSig, clippedSig, declipSig)

    % transpose vectors if needed
    if size( origSig, 1) == size( declipSig, 2)
        declipSig = declipSig';
    elseif size( origSig, 2) == size( declipSig, 1)
        declipSig = declipSig';
    end
    
    % locate all clipped segments
    indexOfClipped = find( abs( clippedSig) == max( abs( clippedSig)));
    
    % perform error calculation at clipped segments
	errors = (origSig( indexOfClipped) - declipSig(indexOfClipped)) .^2;
end