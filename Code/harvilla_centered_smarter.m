% McBAR Declipping Procedure: iteration w/o endfix
%
% Takes a signal, theSignal, and a clipping threshold, tau, and produces
% out, the declipped signal. 
%
% Final iteration of McBAR. Includes:
%
%   > window centering on clipped segments;
%   > dynamic buffer window (max buffer size = 50)
%
function [ out ] = harvilla_centered_smarter( theSignal, tau)

    theSignal = theSignal';
    
    % ensures that large swaths of unclipped data are not considered.
    % setting to 50, up to user.
    maxWindowSize = 50;

    lengthOfSig = length( theSignal);
    
    % find clipping windows
    theRanges = find_ranges( theSignal);
    
    % for each pair of ranges
    out = theSignal;
    for i = 1: 2: length( theRanges)
        
        % initialize shifts
        leftShift = theRanges( i)/2;
        rightShift = (lengthOfSig - theRanges( i+1))/2;
        
        % deal with special cases:
        % here, we can safely look at the last range
        if i ~= 1
            leftShift = ( theRanges( i) - theRanges( i-1))/2;
        end
        
        % here, we can safely look at the next range
        if i+1 ~= length( theRanges)
            rightShift = ( theRanges( i+2) - theRanges( i+1))/2;
        end
        
        % if none of these cases apply, leftShift and rightShift are as
        % they were initialized.
        
        % don't let shifts exceed the specified max buffer size
        leftShift = min( maxWindowSize, floor( leftShift));
        rightShift = min( maxWindowSize, floor( rightShift));
        
        % compute start and end indices, ensuring they do not go
        % past the bounds of the array
        startIndex = max( theRanges(i) - leftShift, 1);
        endIndex = min( theRanges(i+1) + rightShift, lengthOfSig);
        
        % set up the range
        thisRange = startIndex:endIndex;
        
        % declip the signal
        ret = harvilla_declip( theSignal( thisRange), tau);
        
        % build the output
        out( thisRange) = ret;
    end
end

% find_ranges <- a signal A
%
% places and returns windows surrounding clipped segments in A.
function centers = find_ranges( A)

    % need to differentiate between positive and negative clipped regions
    posMax = max( A);
    negMax = min( A);

    centers = [];
    indexInCenters = 1;
    
    startOfInterval = 0;
    lookingThroughInterval = false;
    isPosMax = false;
    isNegMax = false;
    
    for i = 1: length( A)
        
        % if we're not looking through an interval ...
        if ~ lookingThroughInterval
            
            % ... and we have found a positively clipped sample, then:
            if A( i) == posMax
                % flag the other part of the for loop
                lookingThroughInterval = true;
                isPosMax = true; % indicates a positive clipped segment
                startOfInterval = i;
                
            % ... and we have found a negatively clipped sample, then:
            elseif A( i) == negMax
                % flag the other part of the for loop
                lookingThroughInterval = true;
                isNegMax = true; % indicates a negative clipped segment
                startOfInterval = i;
            end
        % otherwise, we're looking through an interval.
        else
            % stop if we encounter a non clipped sample, and record 
            % locations in pairs in indexInCenters.
            if (isPosMax && A( i) ~= posMax) || (isNegMax && A( i) ~= negMax)
                centers( indexInCenters) = startOfInterval;
                centers( indexInCenters+1) = i;
                lookingThroughInterval = false;
                isPosMax = false;
                isNegMax = false;
                indexInCenters = indexInCenters + 2;
            end
        end
    end
end